<html>
<!-- Mirrored from undanganbali.com/Gus Yoga-Gek Widi?name=GusRai by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Dec 2020 08:10:11 GMT -->
<!-- Added by HTTrack -->
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"><!-- /Added by HTTrack -->

	<meta charset="utf-8">
	<!--	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">-->
	<meta data-rh="true" name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<meta property="fb:app_id" content="337950593505989">
	<meta property="og:title" content="Undangan">
	<meta property="og:description"
		  content="Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/Ibu/Saudara/i, berkenan hadir memberikan doa restu kepada kami">
	<meta property="og:image" content="<?= base_url() . 'assets/images/media/cover-1.jpg' ?>">
	<meta property="og:image:width" content="500">
	<meta property="og:url" content="<?= base_url() . 'assets/$order->url' ?>">


	<title>Undangan</title>
	<link rel="stylesheet"
		  href="<?= base_url() . 'assets/frontbase-asset/vendor/bootstrap-4/dist/css/bootstrap.min.css' ?>">
	<link rel="stylesheet" href="<?= base_url() . 'assets/fonts/ionicons.min.css' ?>">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Arvo">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Great+Vibes">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Slab">
	<link rel="stylesheet" href="<?= base_url() . 'assets/css/Footer-Mengundang.css' ?>">
	<link rel="stylesheet" href="<?= base_url() . 'assets/frontbase-asset/vendor/lightbox/ekko-lightbox.css' ?>">
	<link rel="stylesheet" href="<?= base_url() . 'assets/css/Lightbox-Gallery.css' ?>">
	<link rel="stylesheet" href="<?= base_url() . 'assets/css/styles.css' ?>">
	<link rel="stylesheet" href="<?= base_url() . 'assets/css/custom.css' ?>">

	<script src="<?= base_url() . 'assets/frontbase-asset/vendor/jquery/jquery.min.js' ?>"></script>
<body class="" style="">
<header class="py-5 text-center col-12"
		style="background: linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)), url(            <?= base_url() . 'assets/images/media/cover-1.jpg' ?>);background-position: top;background-size: cover;background-repeat: no-repeat;">

	<h3 id="nick-name"
		style="font-family: 'Josefin Slab', serif;font-size: 33px;position: relative;padding-top: 6%;color: rgb(255,255,255);">
		Gus Yoga &amp; Gek Widi </h3>
	<h1 class="title-header">Pawiwahan</h1>
	<h1 style="font-family: Arvo, serif;font-size: 33px;color: rgb(255,255,255);">31 Dec 2020</h1>
	<h4 class="text-uppercase"
		style="font-family: 'Josefin Slab', serif;letter-spacing: 1px;position: relative;color: rgb(255,255,255);">Save
		the date</h4><img src="<?= base_url() . 'assets/img/Line2.png' ?>" style="width: 350px;margin: 0px;"></header>
<section id="bg3" class="col-sm-12 col-md-6 my-5 py-5 mx-auto text-center"><img class="img-fluid"
																				src="<?= base_url() . 'assets/images/media/mid-1.jpg' ?>">
</section>
<section id="event">
	<div class="col-md-8 mx-auto">
		<div class="row my-5">
			<div class="col-md-6 right-border custom-div">
				<h1 class="py-0 my-0 py-lg-5 my-lg-5 custom-size"
					style="font-family: 'Great Vibes', cursive;font-size: 112px;color: rgb(180,169,112);">Kapan?</h1>
			</div>
			<div class="col-md-6">
				<div class="col-12 text-center">
					<h3 class="text-center text-dark" style="font-family: 'Josefin Slab', serif;font-weight: bold;">
						Resepsi Pawiwahan</h3>
					<br>
					<img src="<?= base_url() . 'assets/img/Line.png' ?>" width="200px"
						 style="width: 121px;margin-top: -32px;">
					<h5 class="text-center text-dark" style="font-family: 'Josefin Slab', serif;font-weight: bold;">31
						Desember 2020</h5>
					<p class="my-0">
						10:00 WITA
						-
						Selesai </p>
					<p>Br. Brahmana Bukit, Cempaga, Bangli</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="person" class="col-12">
	<h1 class="pt-5 my-5 text-center">Groom<span>&amp;</span>Bride</h1>
	<div class="row mt-5">
		<div class="container">
			<div class="row">
				<div id="person-indie" class="col-md-6 col-sm-12 mb-5">
					<div class="row text-center">
						<div class="col-12"><img class="img-fluid"
												 src="<?= base_url() . 'assets/images/media/pria.jpg' ?>"></div>
						<div class="col-12 mt-3">
							<h1 style="font-family: 'Great Vibes', cursive;color: rgb(180,169,112);font-size: 70px;font-style: italic;">
								Gus Yoga</h1>
							<h4 class="mb-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida Bagus
								Putu Yogantara, SE</h4>
							<p class="my-0"> Putra pertama dari pasangan</p>
							<h5 class="mt-1 mb-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida
								Bagus Alit Putra</h5>
							<h5 class="my-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">&amp;</h5>
							<h5 class="mt-1" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida Ayu Made
								Karang, S.Pd</h5>
							<h5 class="mt-1" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Br. Brahmana
								Bukit, Cempaga, Bangli</h5>
						</div>
					</div>
				</div>
				<div id="person-indie" class="col-md-6 col-sm-12 mb-5">
					<div class="row text-center">
						<div class="col-12"><img class="img-fluid"
												 src="<?= base_url() . 'assets/images/media/wanita.jpg' ?>"></div>
						<div class="col-12 mt-3">
							<h1 style="font-family: 'Great Vibes', cursive;color: rgb(180,169,112);font-size: 70px;font-style: italic;">
								Gek Widi</h1>
							<h4 class="mb-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida Ayu Made
								Widiadnyani Pertiwi, S.Pd</h4>
							<p class="my-0">Putri kedua dari pasangan</p>
							<h5 class="mt-1 mb-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida Made
								JD Bratha, S.Sos</h5>
							<h5 class="my-0" style="font-family: 'Josefin Slab', serif;font-weight: bold;">&amp;</h5>
							<h5 class="mt-1" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Ida Ayu
								Erawati, S.Pd</h5>
							<h5 class="mt-1" style="font-family: 'Josefin Slab', serif;font-weight: bold;">Griya Kawan,
								Budakeling, Karangasem</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div data-bs-parallax-bg="true" class="my-5 py-5"
	 style="background: transparent; position: relative; overflow: hidden;">
	<div class="justify-content-md-center">
		<div class="col-md-6 col-sm-12 my-3 text-center">
			<p class="ucapan">Merupakan suatu kehormatan dan kebahagiaan kami apabila Bapak/Ibu/Saudara/i , berkenan
				hadir memberikan doa restu kepada kedua mempelai<br></p><img
				src="<?= base_url() . 'assets/img/Line2.png' ?>" style="width: 300px;"></div>
	</div>
	<div
		style="background-image: linear-gradient(0deg, rgba(13, 12, 12, 0.64), rgba(10, 10, 10, 0.81)), url(<?= base_url() . 'assets/images/media/cover-2.jpg' ?>); background-size: cover; background-position: center center; position: absolute; height: 200%; width: 100%; top: 0px; left: 0px; z-index: -100;"></div>
</div>
<div class="photo-gallery desktop-image">
	<div class="container">
		<h1 class="text-center mb-5">Prewedding Moment</h1>
		<div class="row photos">

			<div class="col-4 item"><a href="<?= base_url() . 'assets/images/media/pt1.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/pt1.jpg' ?>"></a>
			</div>
			<div class="col-4 item"><a href="<?= base_url() . 'assets/images/media/pt2.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/pt2.jpg' ?>"></a>
			</div>
			<div class="col-4 item"><a href="<?= base_url() . 'assets/images/media/pt3.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/pt3.jpg' ?>"></a>
			</div>

			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/ls1.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls1.jpg' ?>"></a>
			</div>
			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/ls2.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls2.jpg' ?>"></a>
			</div>
			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/ls3.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls3.jpg' ?>"></a>
			</div>
			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/ls4.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls4.jpg' ?>"></a>
			</div>
			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/3.jpg' ?>"
										data-toggle="lightbox"><img class="img-fluid"
																	src="<?= base_url() . 'assets/images/media/3.jpg' ?>"></a>
			</div>
			<div class="col-6 item"><a href="<?= base_url() . 'assets/images/media/2.jpg' ?>"
										data-toggle="lightbox"><img class="img-fluid"
																	src="<?= base_url() . 'assets/images/media/2.jpg' ?>"></a>
			</div>
		</div>
	</div>
</div>
<div class="photo-gallery mobile-image">
	<div class="container">
		<h1 class="text-center mb-5">Prewedding Moment</h1>
		<div class="row photos">

			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/ls1.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls1.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/ls2.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls2.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/ls3.jpg' ?>"
										data-toggle="lightbox"><img class="img-fluid"
																	src="<?= base_url() . 'assets/images/media/ls3.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/ls4.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/ls4.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/1.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/1.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/2.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/2.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/3.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/3.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/4.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/4.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/5.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/5.jpg' ?>"></a>
			</div>
			<div class="col-12 item"><a href="<?= base_url() . 'assets/images/media/6.jpg' ?>"
									   data-toggle="lightbox"><img class="img-fluid"
																   src="<?= base_url() . 'assets/images/media/6.jpg' ?>"></a>
			</div>
		</div>
	</div>
</div>
<div id="bukutamu" class="container">
	<div class="row">
		<div class="col">
			<h1 class="text-center mb-5">Mohon Doa Restu</h1>
			<script>
				$('#rsvpBtn').on('click', function () {
					var form = $('#rsvpForm').serialize();
					$.ajax({
						type: 'POST',
						url: "<?=base_url() . 'assets/api/rsvp/store'?>",
						data: form,
						beforeSend: function () {
							$("#showLoading").show();
						},
						success: function (response) {
							$('#showNotif').html('Ucapan kakak sudah tersampaikan');
							$('#rsvpForm')[0].reset();
							$('#showNotif').fadeIn('slow');
							setTimeout(function () {
								window.location.href = "index.html";
							}, 2000); //  1000 = 1detik
						},
						complete: function (response) {
							$("#showLoading").hide();
						}
					});
				});
			</script>
		</div>
		<!--			</div>-->
		<!--		</div>-->
	</div>
</div>

<section class="col-12">
	<div class="my-5 py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<a href="https://www.google.com/maps/place/-8.442250,115.355444" data-aos="zoom-in"
					   data-aos-delay="400"
					   class="btn btn-info">Buka Petunjuk Lokasi</a>
					<br>
					<br>
					<!-- Map Section Start -->
					<!--					<section id="google-map-area">-->
					<!--						<div class="container-fluid">-->
					<!--							<div class="row">-->
					<div class="map-responsive">
						<iframe
							src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d986.6465035345717!2d115.35489722917858!3d-8.44224999962057!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zOMKwMjYnMzIuMSJTIDExNcKwMjEnMTkuNiJF!5e0!3m2!1sid!2sid!4v1608106035181!5m2!1sid!2sid"
							width="600" height="400" frameborder="0" style="border:0;" allowfullscreen=""
							aria-hidden="false" tabindex="0"></iframe>
					</div>
					<!--							</div>-->
					<!--						</div>-->
					<!--					</section>-->
					<!-- Map Section End -->
				</div>
			</div>
		</div>
	</div>
</section>

<!--<div class="footer-basic text-center">-->
<!--	<footer>-->
<!--		<div class="social"><a href="https://www.instagram.com/undangan_digitalbali"><i class="icon ion-social-instagram" style="color: rgb(180,169,112);"></i></a><a href="https://www.facebook.com/mengundangkamu"><i class="icon ion-social-facebook" style="color: rgb(180,169,112);"></i></a><a href="https://wa.me/6283114463569"><i class="icon ion-social-whatsapp-outline" style="color: rgb(180,169,112);"></i></a>-->
<!--			<a href="https://www.youtube.com/channel/UCwPLWYwccW3kUPiBB8fRgjw"><i class="icon ion-social-youtube" style="color: rgb(180,169,112);"></i></a>-->
<!--		</div>-->
<!--		<p class="copyright">Mengundangkamu.com © 2019</p>-->
<!--	</footer>-->
<!--</div>-->

<!-- Modal -->

<script type="text/javascript">
	$(window).on('load', function () {
		$('#myModal').modal({backdrop: 'static', keyboard: false});
	});
</script>
<?php if ($nama != null): ?>
	<!-- The Modal -->
	<div class="modal" id="myModal" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="position: relative; top: 30%;">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header" style="border-bottom: none;">
					<button type="button" class="close" onclick="playAudio()" data-dismiss="modal">×</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body text-center pb-5">
					<h5 style="font-family: 'Josefin Slab'; font-size: 27px;">To</h5>
					<h3 style="font-size:60px; border:none;color:#ad8f29; font-family: 'Great Vibes', cursive;"
						class="pt-3"><?= $nama ?></h3>
					<h5 style="font-family: 'Josefin Slab'; font-size: 27px;">From</h5>
					<h5 style="font-family: 'Josefin Slab'; font-size: 27px !important;"> Gus Yoga <span
							style="color:#ad8f29;">&amp;</span> Gek Widi </h5>
				</div>


			</div>
		</div>
	</div>
<?php endif; ?>
<!-- End Modal -->

<audio id="myAudio">
	<source src="<?= base_url() . 'assets/audio/Cintakan%20Membawamu%20-%20Dewa19%20(%20Felix%20Cover%20).mp3' ?>"
			type="audio/mpeg">
	Your browser does not support the audio element.
</audio>
<script>
	var lagu = document.getElementById("myAudio");

	function playAudio() {
		lagu.play();
	}

</script>

<script src="<?= base_url() . 'assets/frontbase-asset/vendor/bootstrap-4/dist/js/bootstrap.min.js' ?>"></script>
<script src="<?= base_url() . 'assets/js/bs-animation.js' ?>"></script>
<script src="<?= base_url() . 'assets/js/Swipe-Slider-7.js' ?>"></script>
<script src="<?= base_url() . 'assets/frontbase-asset/vendor/lightbox/ekko-lightbox.js' ?>"></script>

<script>
	$(document).on('click', '[data-toggle="lightbox"]', function (event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});
</script>


<!-- Mirrored from undanganbali.com/Gus Yoga-Gek Widi?name=GusRai by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Dec 2020 08:11:39 GMT -->
</body>
</html>
